# filebeat
A helm chart for the filebeat agent.

- `Kubernetes kind:` **Daemonset**
- `Image:` **docker.elastic.co/beats/filebeat**
- `App version:` **7.16.2**


The rest of the details are visible in the `values.yaml` file.
