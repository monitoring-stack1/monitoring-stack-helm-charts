# netdata
A helm chart for the netdata-agent.

- `Kubernetes kind:` **Daemonset**
- `Image:` **netdata/netdata**
- `App version:` **v1.31.0**

The rest of the details are visible in the `values.yaml` file.
