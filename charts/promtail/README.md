# promtail
A helm chart for the promtail agent.

- `Kubernetes kind:` **Daemonset**
- `Image:` **grafana/promtail**
- `App version:` **2.3.0**


The rest of the details are visible in the `values.yaml` file.
